# Imports
from flask_script import Manager, prompt_bool
from flask_migrate import Migrate, MigrateCommand

# Import App Components
from bilonz import app, db

manager = Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

# Drop Database
@manager.command
def dropdb():
	if prompt_bool("All data will be deleted, sure you want to proceed?"):
		db.drop_all()
		print ('Dropped the Database')

if __name__ == '__main__':
	manager.run()