# Import Dependencies
from bilonz import db

# Other Dependencies
from datetime import datetime

class User(db.Model):
	name = db.Column(db.String(128), nullable=False)
	email = db.Column(db.String(128), unique=True)
	phone = db.Column(db.String(15), nullable=True)
	password = db.Column(db.String(256), nullable=False)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	private_id = db.Column(db.String(128), primary_key=True)

class Node(db.Model):
	name = db.Column(db.String(32), nullable=False)
	description = db.Column(db.String(2048), nullable=True)
	location = db.Column(db.String(64), nullable=True)
	latitude = db.Column(db.String(64), nullable=True)
	longitude = db.Column(db.String(64), nullable=True)
	elevation = db.Column(db.String(64), nullable=True)
	website = db.Column(db.String(512), nullable=True)
	ipv4 = db.Column(db.String(16), nullable=True)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	private_id = db.Column(db.String(128), primary_key=True)
	# Foreign Key
	user = db.relationship("User", backref=db.backref("node", uselist=False))
	user_id = db.Column(db.String(128), db.ForeignKey('user.private_id'))

class Sensor(db.Model):
	name = db.Column(db.String(32), nullable=False)
	description = db.Column(db.String(256), nullable=True)
	node_id = db.Column(db.String(128), nullable=True)
	lowest = db.Column(db.String(8), nullable=True)
	highest = db.Column(db.String(8), nullable=True)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	private_id = db.Column(db.String(128), primary_key=True)
	# Foreign Key
	user = db.relationship("User", backref=db.backref("sensor", uselist=False))
	user_id = db.Column(db.String(128), db.ForeignKey('user.private_id'))

class Actuator(db.Model):
	name = db.Column(db.String(32), nullable=False)
	description = db.Column(db.String(256), nullable=True)
	node_id = db.Column(db.String(128), nullable=True)
	state = db.Column(db.Boolean, default=0)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	private_id = db.Column(db.String(128), primary_key=True)
	# Foreign Key
	user = db.relationship("User", backref=db.backref("actuator", uselist=False))
	user_id = db.Column(db.String(128), db.ForeignKey('user.private_id'))

class SensorData(db.Model):
	private_id = db.Column(db.String(128), primary_key=True)
	value = db.Column(db.String(32), nullable=False)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	# Foreign Key
	sensor = db.relationship("Sensor", backref=db.backref("sensorData", uselist=False))
	sensor_id = db.Column(db.String(128), db.ForeignKey('sensor.private_id'))

class ActuatorLog(db.Model):
	private_id = db.Column(db.String(128), primary_key=True)
	value = db.Column(db.Boolean, nullable=False)
	agent = db.Column(db.String(32), nullable=True)
	cause = db.Column(db.String(64), nullable=True)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	# Foreign Key
	actuator = db.relationship("Actuator", backref=db.backref("actuatorLog", uselist=False))
	actuator_id = db.Column(db.String(128), db.ForeignKey('actuator.private_id'))

class ActionLookup(db.Model):
	private_id = db.Column(db.String(128), primary_key=True)
	value = db.Column(db.String(32), nullable=False)
	condition = db.Column(db.Boolean, nullable=False)
	action = db.Column(db.Boolean, nullable=False)
	cause = db.Column(db.String(64), nullable=True)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	# Foreign Key
	actuator = db.relationship("Actuator", backref=db.backref("actionLookup", uselist=False))
	actuator_id = db.Column(db.String(128), db.ForeignKey('actuator.private_id'))
	sensor = db.relationship("Sensor", backref=db.backref("actionLookup", uselist=False))
	sensor_id = db.Column(db.String(128), db.ForeignKey('sensor.private_id'))

class PendingEvents(db.Model):
	private_id = db.Column(db.String(128), primary_key=True)
	value = db.Column(db.Boolean, nullable=False)
	agent = db.Column(db.String(32), nullable=True)
	cause = db.Column(db.String(64), nullable=True)
	date = db.Column(db.DateTime, default=datetime.utcnow)
	# Foreign Key
	actuator = db.relationship("Actuator", backref=db.backref("actuatorData", uselist=False))
	actuator_id = db.Column(db.String(128), db.ForeignKey('actuator.private_id'))