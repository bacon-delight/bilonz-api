# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Node, Actuator, PendingEvents, ActuatorLog
from bilonz import db

# Other Dependencies
import uuid


def actuatorInstruction(node_id, actuator_id, actuator_state):

	if not ( actuator_state == '1' or actuator_state == '0' ):
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="States can either be 0 or 1"'}
		)

	# Validate Node
	node = Node.query.filter_by(private_id=node_id).first()
	if not node:
		return make_response(
			'Operation Failed - Invalid Node ID',
			401,
			{'WWW-Authenticate': 'Basic Realm="Invalid Node ID"'}
		)

	# Get the actuator into an object
	actuator = Actuator.query.filter_by(private_id=actuator_id).first()

	# If not could not be found
	if not actuator:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Actuator Not Found"'}
		)

	# Update Node Information in Actuator
	actuator.node_id = node_id

	# Check if Current State in Server is not same as Reporting State
	if int(actuator.state) != int(actuator_state):
		# Update Current Server State
		actuator.state = int(actuator_state)

		# Create a Log
		new_log = ActuatorLog(
			value = int(actuator_state),
			agent = 'ACTUATOR REPORTING',
			cause = 'ACTUATOR TRANSMISSION REPORTED NEW STATE',
			actuator_id = actuator.private_id,
			private_id = str(uuid.uuid4())
		)
		db.session.add(new_log)
		db.session.commit()

	# Get pending action
	action = PendingEvents.query.filter_by(actuator_id=actuator_id).first()

	# If no Action
	if not action:
		db.session.commit()
		# Generate Response
		return make_response(
			jsonify(
				{
					'action': True,
					'message': 'Actuator State Retrieved Successfully',
					'Content-Type': 'application/json; charset=utf-8',
					'changeRequired': False,
					'change': ''
				}
			)
		)
		
	# If Action Needed
	change = action.value
	changeRequired = False
	stateComment = ' (ACTUATOR REPORTED DESIRED STATE)'
	if int(action.value) != int(actuator_state):
		changeRequired = True
		stateComment = ' (ACTUATOR FETCHED NEW STATE)'

	# Create a Log
	new_log = ActuatorLog(
		value = int(change),
		agent = action.agent,
		cause = action.cause + stateComment,
		actuator_id = actuator.private_id,
		private_id = str(uuid.uuid4())
	)

	# Commit in Database
	db.session.delete(action)
	db.session.add(new_log)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator State Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'changeRequired': changeRequired,
				'change': change
			}
		)
	)