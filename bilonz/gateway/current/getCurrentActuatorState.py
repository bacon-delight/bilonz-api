# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Actuator

def getCurrentActuatorState(actuator_id):

	# Get the actuator into an object
	actuator = Actuator.query.filter_by(private_id=actuator_id).first()

	# If not could not be found
	if not actuator:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Actuator Not Found"'}
		)

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator State Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'value': actuator.state
			}
		)
	)