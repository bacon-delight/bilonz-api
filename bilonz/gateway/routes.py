# Flask Components
from flask import Blueprint, request, make_response

# Other Dependencies
from functools import wraps

# Internal Dependencies
from bilonz.models import Node

# Route  Dependencies
from bilonz.gateway.payload.getPayload import getPayload
from bilonz.gateway.instruction.actuatorInstruction import actuatorInstruction
from bilonz.gateway.transmit.sensorTransmission import sensorTransmission

# Register Blueprint
module = Blueprint('gateway', __name__)

# Decorator - Node ID Required
def validate_payload(f):
	@wraps(f)
	def decorated(*args, **kwargs):

		# Check for Node ID in Header
		node_id = None
		if 'x-access-node' in request.headers:
			node_id = request.headers['x-access-node']
		if not node_id:
			return make_response(
				'Operation Failed - Node ID Missing',
				401,
				{'WWW-Authenticate': 'Basic Realm="Node ID Missing in Header"'}
			)
		
		# Validate Node
		node = Node.query.filter_by(private_id=node_id).first()
		if not node:
			return make_response(
				'Operation Failed - Invalid Node ID',
				401,
				{'WWW-Authenticate': 'Basic Realm="Invalid Node ID"'}
			)	

		return f(node, *args, **kwargs)
	
	return decorated


# Actuator Instruction Read
@module.route('/instruction/<node_id>/<actuator_id>/<actuator_state>', methods=['GET'])
def get_actuator_instruction(node_id, actuator_id, actuator_state):
	return actuatorInstruction(node_id, actuator_id, actuator_state), 200

# Sensor Transmission
@module.route('/transmit/<node_id>/<sensor_id>/<sensor_value>', methods=['GET'])
def get_sensor_value(node_id, sensor_id, sensor_value):
	return sensorTransmission(node_id, sensor_id, sensor_value), 200

# Node Interface Route
@module.route('/payload', methods=['POST'])
@validate_payload
def get_payload(node):
	payload = request.get_json()
	return getPayload(node, payload)