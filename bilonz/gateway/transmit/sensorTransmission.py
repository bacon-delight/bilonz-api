# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Node, Sensor, SensorData, ActionLookup, Actuator, PendingEvents
from bilonz import db

def sensorTransmission(node_id, sensor_id, sensor_value):

	# Validate Node
	node = Node.query.filter_by(private_id=node_id).first()
	if not node:
		return make_response(
			'Operation Failed - Invalid Node ID',
			401,
			{'WWW-Authenticate': 'Basic Realm="Invalid Node ID"'}
		)

	# Get the sensor into an object
	sensor = Sensor.query.filter_by(private_id=sensor_id).first()

	# If not could not be found
	if not sensor:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Sensor Not Found"'}
		)

	# Update Node Information in Sensor
	sensor.node_id = node_id

	try:
		float(sensor_value)
	except ValueError:
		return make_response(
			'Operation Failed - Integers or Floating Point Accepted Only',
			401,
			{'WWW-Authenticate': 'Basic Realm="Integers or Floating Point Accepted Only"'}
		)

	# Insert Into Database
	sensor_data = SensorData(
		private_id = str(uuid.uuid4()),
		value = float(sensor_value),
		sensor_id = sensor_id
	)

	# Check for Actions in Action Lookup
	actions = ActionLookup.query.filter_by(sensor_id=sensor_id).all()
	
	if actions:
		for action in actions:
			# Fetch Actuator Data
			actuator = Actuator.query.filter_by(private_id=action.actuator_id).first()
			
			# Greater Than Condition
			if action.condition == True:
				if float(sensor_value) > float(action.value):
					# Delete Old Pending Events
					oldEvents = PendingEvents.query.filter_by(actuator_id=action.actuator_id).delete()
					# Check if Actuator State is Currently the Desired State
					if actuator.state != int(action.action):
						# New PendingEvents Object
						new_event = PendingEvents(
							value = int(action.action),
							actuator_id = action.actuator_id,
							agent = 'SENSOR REPORTING',
							cause = 'SENSOR DATA GREATER THAN USER APPLIED CONDITION',
							private_id = str(uuid.uuid4())
						)
						db.session.add(new_event)
			
			# Less Than Condition
			else:
				if float(sensor_value) < float(action.value):
					# Delete Old Pending Events
					oldEvents = PendingEvents.query.filter_by(actuator_id=action.actuator_id).delete()
					# Check if Actuator State is Currently the Desired State
					if actuator.state != int(action.action):
						# New PendingEvents Object
						new_event = PendingEvents(
							value = int(action.action),
							actuator_id = action.actuator_id,
							agent = 'SENSOR REPORTING',
							cause = 'SENSOR DATA LESS THAN USER APPLIED CONDITION',
							private_id = str(uuid.uuid4())
						)
						db.session.add(new_event)
	
	# Add Sensor Data to the Database
	db.session.add(sensor_data)

	# Commit Insertions (If Any)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Sensor State Received and Archived Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'acknowledgement': sensor_data.private_id
			}
		)
	)