# Flask Components
from flask import make_response, jsonify

# Internal Dependencies
from bilonz.gateway.payload.handleSensorPayload import handleSensorPayload
from bilonz.gateway.payload.handleActuatorPayload import handleActuatorPayload

def getPayload(node, payload):

	# Check Existence of Payload
	if not payload:
		return make_response(
			'Operation Failed - Payload Missing',
			401,
			{'WWW-Authenticate': 'Basic Realm="Missing Payload"'}
		)

	else:
		# Response
		responseAck = []

		'''
			Handle Sensor Data
		'''
		if 'sensors' in payload:
			# Handle Sensor Payload
			sensorAck = handleSensorPayload(node, payload['sensors'])
			responseAck.append({'sensors': sensorAck})
		else:
			# Response
			responseAck.append({'sensors': 'No Data Received'})
		#endif

		'''
			Handle Actuator Data
		'''
		if 'actuators' in payload:
			# Handle Actuator Payload
			actuatorAck = handleActuatorPayload(node, payload['actuators'])
			responseAck.append({'actuators': actuatorAck})
		else:
			# Response
			responseAck.append({'actuators': 'No Data Received'})
		#endif

	#endif

	# Response Acknowledgement
	return jsonify({'response': responseAck})