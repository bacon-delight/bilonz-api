# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Actuator, ActuatorLog
from bilonz import db

def handleActuatorPayload(node, payload):

	# Response
	actuatorAck = []

	# Validate Actuators
	for actuators in payload:
		# If Actuator ID is Invalid (No actuators exists with that ID)
		actuator = Actuator.query.filter_by(private_id=actuators['id']).first()
		if not actuator:
			# Response
			actuatorResponse = {}
			actuatorResponse['error'] = True
			actuatorResponse['success'] = False
			actuatorResponse['actuator_id'] = actuators['id']
			actuatorResponse['message'] = 'Invalid Actuator ID'
		else:
			# Check if the Actuator belongs to a different user
			if actuator.user_id != node.user_id:
				# Response
				actuatorResponse = {}
				actuatorResponse['error'] = True
				actuatorResponse['success'] = False
				actuatorResponse['actuator_id'] = actuators['id']
				actuatorResponse['message'] = 'User associated with the actuator and node are different'
			else:
				# Check Actuator Value
				if not ( actuators['value'] == True or actuators['value'] == False ):
					# Response
					actuatorResponse = {}
					actuatorResponse['error'] = True
					actuatorResponse['success'] = False
					actuatorResponse['actuator_id'] = actuators['id']
					actuatorResponse['message'] = 'Actuator Value can only be boolean (0/1)'
				else:
					# Insert Into Database
					actuator_data = ActuatorLog(
						private_id = str(uuid.uuid4()),
						value = actuators['value'],
						actuator_id = actuator.private_id
					)

					# Add Actuator Data to the Database
					db.session.add(actuator_data)

					# Update Node ID in Actuator
					actuator.node_id = node.private_id
					
					# Response
					actuatorResponse = {}
					actuatorResponse['error'] = False
					actuatorResponse['success'] = True
					actuatorResponse['actuator_id'] = actuators['id']
					actuatorResponse['message'] = 'Data Inserted Successfully'
				#endif
			#endif
		#endif
		actuatorAck.append(actuatorResponse)
	#endfor

	# Commit Insertions (If Any)
	db.session.commit()

	# Return Ack
	return actuatorAck