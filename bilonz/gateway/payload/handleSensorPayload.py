# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Sensor, SensorData
from bilonz import db

def handleSensorPayload(node, payload):

	# Response
	sensorAck = []

	# Validate Sensors
	for sensors in payload:
		# If Sensor ID is Invalid (No sensors exists with that ID)
		sensor = Sensor.query.filter_by(private_id=sensors['id']).first()
		if not sensor:
			# Response
			sensorResponse = {}
			sensorResponse['error'] = True
			sensorResponse['success'] = False
			sensorResponse['sensor_id'] = sensors['id']
			sensorResponse['message'] = 'Invalid Sensor ID'
		else:
			# Check if the Sensor belongs to a different user
			if sensor.user_id != node.user_id:
				# Response
				sensorResponse = {}
				sensorResponse['error'] = True
				sensorResponse['success'] = False
				sensorResponse['sensor_id'] = sensors['id']
				sensorResponse['message'] = 'User associated with the sensor and node are different'
			else:
				# Insert Into Database
				sensor_data = SensorData(
					private_id = str(uuid.uuid4()),
					value = sensors['value'],
					sensor_id = sensor.private_id
				)
				
				# Add Sensor Data to the Database
				db.session.add(sensor_data)

				# Update Node ID in Sensor
				sensor.node_id = node.private_id
				
				# Response
				sensorResponse = {}
				sensorResponse['error'] = False
				sensorResponse['success'] = True
				sensorResponse['sensor_id'] = sensors['id']
				sensorResponse['message'] = 'Data Inserted Successfully'
			#endif
		#endif
		sensorAck.append(sensorResponse)
	#endfor

	# Commit Insertions (If Any)
	db.session.commit()

	# Return Ack
	return sensorAck