# Flask Components
from flask import Blueprint, request, make_response

# Other Dependencies
from functools import wraps
import jwt

# Internal Dependencies
from bilonz.models import User
from bilonz import app

# Register Blueprint
module = Blueprint('subsystems', __name__)

# Decorator - Token Required (Checks authenticity of current user)
def token_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		token = None
		if 'x-access-token' in request.headers:
			token = request.headers['x-access-token']
		if not token:
			return make_response(
				'Operation Failed - Token Missing',
				401,
				{'WWW-Authenticate': 'Basic Realm="Token Missing"'}
			)
		try:
			data = jwt.decode(token, app.config['SECRET_KEY'])
			current_user = User.query.filter_by(private_id=data['private_id']).first()
		except:
			return make_response(
				'Operation Failed - Token Invalid',
				401,
				{'WWW-Authenticate': 'Basic Realm="Token Invalid"'}
			)
		return f(current_user, *args, **kwargs)
	return decorated

import bilonz.subsystems.nodeRoutes
import bilonz.subsystems.sensorRoutes
import bilonz.subsystems.actuatorRoutes
import bilonz.subsystems.actionRoutes
import bilonz.subsystems.refreshRoutes

# Validate Token
@module.route('/validate', methods=['GET'])
@token_required
def validate_token(current_user):
	# Response
	return make_response(
		'Token Valid',
		200,
		{'WWW-Authenticate': 'Basic Realm="Token is Valid"'}
	)