# Flask Components
from flask import request

# Import Decorators and Module Components
from bilonz.subsystems.routes import module, token_required

# Internal Dependencies
from bilonz.subsystems.refresh.sensor import sensor
from bilonz.subsystems.refresh.actuator import actuator


# Last Sensor Reading
@module.route('/refresh/sensor/<sensor_id>', methods=['GET'])
@token_required
def get_last_sensor_reading(current_user, sensor_id):
	return sensor(current_user, sensor_id)

# Current Actuator State
@module.route('/refresh/actuator/<actuator_id>', methods=['GET'])
@token_required
def get_current_actuator_state(current_user, actuator_id):
	return actuator(current_user, actuator_id)