# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import ActionLookup
from bilonz import db

def revokeRule(rule_id):

	# Search for Existing Rule
	rule = ActionLookup.query.filter_by(private_id=rule_id).first()
	if not rule:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Rule Not Found"'}
		)

	# Commit in Database
	db.session.delete(rule)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Rule Deleted Successfully',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)