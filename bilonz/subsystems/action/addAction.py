# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Actuator, Sensor, ActionLookup
from bilonz import db

def addAction(current_user, data):
	
	# If parameters are missing
	if not data or not data['actuator_id'] or not data['sensor_id']:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing (Actuator ID and New State are both Required)"'}
		)

	# Search for Existing Actuator
	actuator = Actuator.query.filter_by(private_id=data['actuator_id']).first()
	if not actuator:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Actuator ID Incorrect"'}
		)

	# Check if the Actuator belongs to a different user
	if actuator.user_id != current_user.private_id:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="User associated with the actuator is different"'}
		)

	# Search for Existing Sensor
	sensor = Sensor.query.filter_by(private_id=data['sensor_id']).first()
	if not sensor:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Sensor ID Incorrect"'}
		)

	# Check if the Sensor belongs to a different user
	if sensor.user_id != current_user.private_id:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="User associated with the sensor is different"'}
		)

	# Find and drop any previous events associated with the same actuator and sensor
	# oldEvents = ActionLookup.query.filter_by(actuator_id=data['actuator_id'], sensor_id=data['sensor_id']).delete()

	# New ActionLookup Object
	new_action = ActionLookup(
		value = data['value'],
		condition = int(data['condition']),
		action = int(data['action']),
		cause = data['cause'],
		actuator_id = data['actuator_id'],
		sensor_id = data['sensor_id'],
		private_id = str(uuid.uuid4())
	)

	# Commit in Database
	db.session.add(new_action)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Action Event Added in Lookup',
				'Content-Type': 'application/json; charset=utf-8',
				'rule_id': new_action.private_id
			}
		)
	)