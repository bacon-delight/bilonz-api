# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Actuator, PendingEvents
from bilonz import db

def executeAction(current_user, actuator_id, actuator_value):

	# Search for Existing Actuator
	actuator = Actuator.query.filter_by(private_id=actuator_id).first()
	if not actuator:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Actuator ID Incorrect"'}
		)

	# Check if the Actuator belongs to a different user
	if actuator.user_id != current_user.private_id:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="User associated with the actuator is different"'}
		)

	# Find and drop any previous events associated with the same actuator
	oldEvents = PendingEvents.query.filter_by(actuator_id=actuator_id).delete()

	# If current state of actuator is equal to new issues state
	if actuator.state == int(actuator_value):
		# Generate Response
		return make_response(
			jsonify(
				{
					'action': True,
					'message': 'Action Event Ignored as Current Actuator State Matches Issued State',
					'Content-Type': 'application/json; charset=utf-8'
				}
			)
		)

	# New PendingEvents Object
	new_event = PendingEvents(
		value = int(actuator_value),
		actuator_id = actuator_id,
		agent = 'USER THROUGH WEB GUI',
		cause = 'USER ACTION',
		private_id = str(uuid.uuid4())
	)

	# Commit in Database
	db.session.add(new_event)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Action Event Added',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)