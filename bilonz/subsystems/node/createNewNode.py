# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Node
from bilonz import db

def createNewNode(current_user, data):
	
	# If parameters are missing
	if not data or not data['name']:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing (Node Name Required)"'}
		)

	# New Node Object
	new_node = Node(
		name = data['name'],
		description = data['description'],
		location = data['location'],
		latitude = data['latitude'],
		longitude = data['longitude'],
		elevation = data['elevation'],
		website = data['website'],
		private_id = str(uuid.uuid4()),
		user_id = current_user.private_id
	)

	# Commit in Database
	db.session.add(new_node)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Node Created Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'private_id': new_node.private_id,
				'location': new_node.location
			}
		)
	)