# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Node

def getAllNodes(current_user):

	# Get the node into an object
	nodes = Node.query.filter_by(user_id=current_user.private_id).all()

	# If not could not be found
	if not nodes:
		return make_response(
				jsonify(
				{
					'action': True,
					'exists': False,
					'message': 'No Existing Nodes',
					'Content-Type': 'application/json; charset=utf-8'
				}
			)
		)

	# Output Object
	output = []
	for node in nodes:
		node_data = {}
		node_data['name'] = node.name
		node_data['description'] = node.description
		node_data['location'] = node.location
		node_data['latitude'] = node.latitude
		node_data['longitude'] = node.longitude
		node_data['ipv4'] = node.ipv4
		node_data['private_id'] = node.private_id
		node_data['website'] = node.website
		output.append(node_data)

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'exists': True,
				'message': 'Nodes Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'nodes': output
			}
		)
	)