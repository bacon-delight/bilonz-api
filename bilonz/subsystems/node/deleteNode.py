# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Node
from bilonz import db

def deleteNode(current_user, node_id):

	# Get the node into an object
	node = Node.query.filter_by(private_id=node_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not node:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Node Not Found"'}
		)

	# Commit into Database
	db.session.delete(node)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Node Delted Successfully',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)