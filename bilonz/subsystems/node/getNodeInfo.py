# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Node

def getNodeInfo(current_user, node_id):

	# Get the node into an object
	node = Node.query.filter_by(private_id=node_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not node:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Node Not Found"'}
		)

	# Output Object
	output = {}
	output['name'] = node.name
	output['description'] = node.description
	output['location'] = node.location
	node_data['latitude'] = node.latitude
	node_data['longitude'] = node.longitude
	output['ipv4'] = node.ipv4
	output['private_id'] = node.private_id
	node_data['website'] = node.website

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Node Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'node': output
			}
		)
	)