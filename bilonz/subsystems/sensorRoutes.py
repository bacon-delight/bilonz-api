# Flask Components
from flask import request

# Import Decorators and Module Components
from bilonz.subsystems.routes import module, token_required

# Internal Dependencies
from bilonz.subsystems.sensor.createNewSensor import createNewSensor
from bilonz.subsystems.sensor.getAllSensors import getAllSensors
from bilonz.subsystems.sensor.updateSensor import updateSensor
from bilonz.subsystems.sensor.deleteSensor import deleteSensor
from bilonz.subsystems.sensor.getSensorInfo import getSensorInfo


# Get Sensor Information
@module.route('/sensor/<sensor_id>', methods=['GET'])
@token_required
def get_sensor_info(current_user, sensor_id):
	# Retrieve Sensor Information
	return getSensorInfo(current_user, sensor_id)

# Get All Sensors for Current User
@module.route('/sensor', methods=['GET'])
@token_required
def get_all_sensors(current_user):
	# Retrieve All Sensors
	return getAllSensors(current_user)

# Create New Sensor
@module.route('/sensor', methods=['POST'])
@token_required
def create_new_sensor(current_user):
	# Get POST Body
	data = request.get_json()
	# Create New Sensor
	return createNewSensor(current_user, data)

# Edit Sensor Information
@module.route('/sensor/<sensor_id>', methods=['PUT'])
@token_required
def update_sensor(current_user, sensor_id):
	# Get PUT Body
	data = request.get_json()
	# Edit Sensor Information
	return updateSensor(current_user, sensor_id, data)

# Delete Sensor
@module.route('/sensor/<sensor_id>', methods=['DELETE'])
@token_required
def delete_sensor(current_user, sensor_id):
	# Delete Sensor
	return deleteSensor(current_user, sensor_id)