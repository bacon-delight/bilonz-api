# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Actuator, ActuatorLog

def getActuatorInfo(current_user, actuator_id):

	# Get the actuator into an object
	actuator = Actuator.query.filter_by(private_id=actuator_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not actuator:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Actuator Not Found"'}
		)

	# Retrieve Actuator Data
	actuatorLog = []
	rows = ActuatorLog.query.filter_by(actuator_id=actuator_id).all()
	print(rows)
	for row in rows:
		data = {}
		data['value'] = row.value
		data['date'] = row.date
		data['agent'] = row.agent
		data['cause'] = row.cause
		actuatorLog.append(data)

	# Output Object
	actuatorInfo = {}
	actuatorInfo['name'] = actuator.name
	actuatorInfo['description'] = actuator.description
	actuatorInfo['private_id'] = actuator.private_id
	actuatorInfo['state'] = actuator.state
	actuatorInfo['node_id'] = actuator.node_id

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'actuator_info': actuatorInfo,
				'actuator_log': actuatorLog
			}
		)
	)