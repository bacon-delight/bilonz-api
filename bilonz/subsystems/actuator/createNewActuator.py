# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Actuator
from bilonz import db

def createNewActuator(current_user, data):
	
	# If parameters are missing
	if not data or not data['name']:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing (Actuator Name Required)"'}
		)

	# New Actuator Object
	new_actuator = Actuator(
		name = data['name'],
		description = data['description'],
		private_id = str(uuid.uuid4()),
		user_id = current_user.private_id
	)

	# Commit in Database
	db.session.add(new_actuator)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator Created Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'private_id': new_actuator.private_id
			}
		)
	)