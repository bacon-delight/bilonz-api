# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Actuator
from bilonz import db

def deleteActuator(current_user, actuator_id):

	# Get the Actuator into an object
	actuator = Actuator.query.filter_by(private_id=actuator_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not actuator:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Actuator Not Found"'}
		)

	# Commit into Database
	db.session.delete(actuator)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator Delted Successfully',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)