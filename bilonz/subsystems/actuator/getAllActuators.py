# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Actuator

def getAllActuators(current_user):

	# Get the actuator into an object
	actuators = Actuator.query.filter_by(user_id=current_user.private_id).all()

	# If not could not be found
	if not actuators:
		return make_response(
				jsonify(
				{
					'action': True,
					'exists': False,
					'message': 'No Existing Actuators',
					'Content-Type': 'application/json; charset=utf-8'
				}
			)
		)

	# Output Object
	output = []
	for actuator in actuators:
		actuator_data = {}
		actuator_data['name'] = actuator.name
		actuator_data['description'] = actuator.description
		actuator_data['node_id'] = actuator.node_id
		actuator_data['state'] = actuator.state
		actuator_data['private_id'] = actuator.private_id
		output.append(actuator_data)

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'exists': True,
				'message': 'Actuators Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'actuators': output
			}
		)
	)