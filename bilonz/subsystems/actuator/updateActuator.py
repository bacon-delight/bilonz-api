# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Actuator
from bilonz import db

def updateActuator(current_user, actuator_id, data):
	
	# If parameters are missing
	if not data or not data['name']:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing (Actuator Name Required)"'}
		)

	# Get the actuator into an object
	actuator = Actuator.query.filter_by(private_id=actuator_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not actuator:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Actuator Not Found"'}
		)

	# Update Actuator information
	actuator.name = data['name']
	actuator.description = data['description']

	# Commit into Database
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator Updated Successfully',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)