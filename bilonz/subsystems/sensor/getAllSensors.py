# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Sensor

def getAllSensors(current_user):

	# Get the sensor into an object
	sensors = Sensor.query.filter_by(user_id=current_user.private_id).all()

	# If not could not be found
	if not sensors:
		return make_response(
				jsonify(
				{
					'action': True,
					'exists': False,
					'message': 'No Existing Sensors',
					'Content-Type': 'application/json; charset=utf-8'
				}
			)
		)

	# Output Object
	output = []
	for sensor in sensors:
		sensor_data = {}
		sensor_data['name'] = sensor.name
		sensor_data['description'] = sensor.description
		sensor_data['node_id'] = sensor.node_id
		sensor_data['private_id'] = sensor.private_id
		output.append(sensor_data)

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'exists': True,
				'message': 'Sensors Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'sensors': output
			}
		)
	)