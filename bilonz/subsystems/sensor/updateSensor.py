# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Sensor
from bilonz import db

def updateSensor(current_user, sensor_id, data):
	
	# If parameters are missing
	if not data or not data['name']:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing (Sensor Name Required)"'}
		)

	# Get the sensor into an object
	sensor = Sensor.query.filter_by(private_id=sensor_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not sensor:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Sensor Not Found"'}
		)

	# Update sensor information
	sensor.name = data['name']
	sensor.description = data['description']
	sensor.lowest = data['lowest']
	sensor.highest = data['highest']

	# Commit into Database
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Sensor Updated Successfully',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)