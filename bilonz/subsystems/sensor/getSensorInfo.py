# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Sensor, SensorData, ActionLookup

def getSensorInfo(current_user, sensor_id):

	# Get the sensor into an object
	sensor = Sensor.query.filter_by(private_id=sensor_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not sensor:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Sensor Not Found"'}
		)

	# Retrieve Sensor Data
	sensorData = []
	rows = SensorData.query.filter_by(sensor_id=sensor_id).all()
	for row in rows:
		data = {}
		data['value'] = row.value
		data['date'] = row.date
		data['record_id'] = row.private_id
		sensorData.append(data)

	# Output Object
	sensorInfo = {}
	sensorInfo['name'] = sensor.name
	sensorInfo['description'] = sensor.description
	sensorInfo['private_id'] = sensor.private_id
	sensorInfo['node_id'] = sensor.node_id
	sensorInfo['lowest'] = sensor.lowest
	sensorInfo['highest'] = sensor.highest

	actions = ActionLookup.query.filter_by(sensor_id=sensor_id).all()

	actionData = []
	if actions:
		for action in actions:
			data = {}
			data['actuator_id'] = action.actuator_id
			data['value'] = action.value
			data['action'] = action.action
			data['condition'] = action.condition
			data['rule_id'] = action.private_id
			actionData.append(data)

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Sensor Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'sensor_info': sensorInfo,
				'data': sensorData,
				'actions': actionData
			}
		)
	)