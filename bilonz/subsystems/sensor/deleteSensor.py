# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Sensor, SensorData
from bilonz import db

def deleteSensor(current_user, sensor_id):

	# Get the sensor into an object
	sensor = Sensor.query.filter_by(private_id=sensor_id, user_id=current_user.private_id).first()

	# If not could not be found
	if not sensor:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Sensor Not Found"'}
		)

	# Delete Sensor Data
	sensorData = SensorData.query.filter_by(sensor_id=sensor_id).delete()

	# Commit into Database
	db.session.delete(sensor)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Sensor Delted Successfully',
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)