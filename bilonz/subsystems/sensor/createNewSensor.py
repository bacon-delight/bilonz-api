# Flask Components
from flask import jsonify, make_response

# Other Dependencies
import uuid

# Internal Dependencies
from bilonz.models import Sensor
from bilonz import db

def createNewSensor(current_user, data):
	print (data)
	# If parameters are missing
	if not data or not data['name']:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing (Sensor Name Required)"'}
		)

	# New Sensor Object
	new_sensor = Sensor(
		name = data['name'],
		description = data['description'],
		private_id = str(uuid.uuid4()),
		user_id = current_user.private_id,
		lowest = data['lowest'],
		highest = data['highest']
	)

	# Commit in Database
	db.session.add(new_sensor)
	db.session.commit()

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Sensor Created Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'private_id': new_sensor.private_id
			}
		)
	)