# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Actuator

def actuator(current_user, actuator_id):

	# Get the actuator into an object
	actuator = Actuator.query.filter_by(private_id=actuator_id).first()

	# If not could not be found
	if not actuator:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Actuator Not Found"'}
		)

	# If actuator belongs to different user
	if actuator.user_id != current_user.private_id:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Different User Associated with the Actuator"'}
		)

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Actuator State Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'value': actuator.state
			}
		)
	)