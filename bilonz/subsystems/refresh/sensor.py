# Flask Components
from flask import jsonify, make_response

# Internal Dependencies
from bilonz.models import Sensor, SensorData

def sensor(current_user, sensor_id):

	# Get the sensor into an object
	sensor = Sensor.query.filter_by(private_id=sensor_id).first()

	# If not could not be found
	if not sensor:
		return make_response(
			'Operation Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Sensor Not Found"'}
		)

	# Check if Sensor Belongs to Current User
	if sensor.user_id != current_user.private_id:
		return make_response(
			'Operation Failed',
			403,
			{'WWW-Authenticate': 'Basic Realm="Different User Associated with the Sensor"'}
		)

	# Retrieve Sensor Data
	row = SensorData.query.filter_by(sensor_id=sensor_id).order_by('date DESC').first()
	value = ''
	date = ''
	record_id = ''
	if row:
		value = row.value
		date = row.date
		record_id = row.private_id

	# Generate Response
	return make_response(
		jsonify(
			{
				'action': True,
				'message': 'Sensor Retrieved Successfully',
				'Content-Type': 'application/json; charset=utf-8',
				'date': date,
				'value': value,
				'record_id': record_id
			}
		)
	)