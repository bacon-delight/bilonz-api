# Flask Components
from flask import request

# Import Decorators and Module Components
from bilonz.subsystems.routes import module, token_required

# Internal Dependencies
from bilonz.subsystems.action.executeAction import executeAction
from bilonz.subsystems.action.addAction import addAction
from bilonz.subsystems.action.revokeRule import revokeRule


# Execute New Action
@module.route('/action/<actuator_id>/<actuator_value>', methods=['GET'])
@token_required
def execute_action(current_user, actuator_id, actuator_value):
	# Create New Actuator
	return executeAction(current_user, actuator_id, actuator_value)

# Create New Action Lookup
@module.route('/action', methods=['POST'])
@token_required
def add_action(current_user):
	# Get POST Body
	data = request.get_json()
	# Create New Actuator
	return addAction(current_user, data)

# Delete Rule
@module.route('/action/<rule_id>', methods=['Delete'])
@token_required
def revoke_rule(current_user, rule_id):
	# Create New Actuator
	return revokeRule(rule_id)