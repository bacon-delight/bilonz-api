# Flask Components
from flask import request

# Import Decorators and Module Components
from bilonz.subsystems.routes import module, token_required

# Internal Dependencies
from bilonz.subsystems.node.createNewNode import createNewNode
from bilonz.subsystems.node.updateNode import updateNode
from bilonz.subsystems.node.deleteNode import deleteNode
from bilonz.subsystems.node.getNodeInfo import getNodeInfo
from bilonz.subsystems.node.getAllNodes import getAllNodes

# Get Node Information
@module.route('/node/<node_id>', methods=['GET'])
@token_required
def get_node_info(current_user, node_id):
	# Retrieve Node Information
	return getNodeInfo(current_user, node_id)

# Get All Nodes for Current User
@module.route('/node', methods=['GET'])
@token_required
def get_all_nodes(current_user):
	# Retrieve All Nodes
	return getAllNodes(current_user)

# Create New Node
@module.route('/node', methods=['POST'])
@token_required
def create_new_node(current_user):
	# Get POST Body
	data = request.get_json()
	# Create New Node
	return createNewNode(current_user, data)

# Edit Node Information
@module.route('/node/<node_id>', methods=['PUT'])
@token_required
def update_node(current_user, node_id):
	# Get PUT Body
	data = request.get_json()
	# Edit Node Information
	return updateNode(current_user, node_id, data)

# Delete Node
@module.route('/node/<node_id>', methods=['DELETE'])
@token_required
def delete_node(current_user, node_id):
	# Delete Node
	return deleteNode(current_user, node_id)