# Flask Components
from flask import request

# Import Decorators and Module Components
from bilonz.subsystems.routes import module, token_required

# Internal Dependencies
from bilonz.subsystems.actuator.createNewActuator import createNewActuator
from bilonz.subsystems.actuator.getAllActuators import getAllActuators
from bilonz.subsystems.actuator.updateActuator import updateActuator
from bilonz.subsystems.actuator.deleteActuator import deleteActuator
from bilonz.subsystems.actuator.getActuatorInfo import getActuatorInfo


# Get Actuator Information
@module.route('/actuator/<actuator_id>', methods=['GET'])
@token_required
def get_actuator_info(current_user, actuator_id):
	# Retrieve Actuator Information
	return getActuatorInfo(current_user, actuator_id)

# Get All Actuators for Current User
@module.route('/actuator', methods=['GET'])
@token_required
def get_all_actuators(current_user):
	# Retrieve All Actuators
	return getAllActuators(current_user)

# Create New Actuator
@module.route('/actuator', methods=['POST'])
@token_required
def create_new_actuator(current_user):
	# Get POST Body
	data = request.get_json()
	# Create New Actuator
	return createNewActuator(current_user, data)

# Edit Actuator Information
@module.route('/actuator/<actuator_id>', methods=['PUT'])
@token_required
def update_actuator(current_user, actuator_id):
	# Get PUT Body
	data = request.get_json()
	# Edit Actuator Information
	return updateActuator(current_user, actuator_id, data)

# Delete Actuator
@module.route('/actuator/<actuator_id>', methods=['DELETE'])
@token_required
def delete_actuator(current_user, actuator_id):
	# Delete Actuator
	return deleteActuator(current_user, actuator_id)