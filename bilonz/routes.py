# Flask Components
from flask import make_response
from bilonz import app

# Page Not Found
@app.errorhandler(404)
def page_not_found(e):
	return make_response(
		'Page Not Found',
		404,
		{'WWW-Authenticate': 'Basic Realm="Invalid URL"'}
	)

# Bad Request
@app.errorhandler(400)
def page_not_found(e):
	return make_response(
		'Bad Request',
		400,
		{'WWW-Authenticate': 'Basic Realm="HTTP Request Issue"'}
	)

# Forbidden
@app.errorhandler(403)
def forbidden_action(e):
	return make_response(
		'Forbidden Action',
		403,
		{'WWW-Authenticate': 'Basic Realm="Action/Request Forbidden"'}
	)

# Internal Server Error
@app.errorhandler(500)
def internal_server_error(e):
	return make_response(
		'Internal Server Error',
		500,
		{'WWW-Authenticate': 'Basic Realm="Internal Server Error"'}
	)