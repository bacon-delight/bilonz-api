# Import Flask Modules
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin

# Import Other Dependencies
import os

# Flask App
app = Flask(__name__)
CORS(app)

# Setup Base Directory
basedir = os.path.abspath(os.path.dirname(__file__))

# Configure App and Database
app.config['SECRET_KEY'] = '\x9b2\xe23Hh\x0c\xa6\x82\xb0\xba\x90\ts\x10h\xd3-\x02\x91\x8a\xa5\xc6\x16'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'bilonz.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['DEBUG'] = True
db = SQLAlchemy(app)

import bilonz.models

# Import Blueprints
from bilonz.auth.routes import module
from bilonz.subsystems.routes import module
from bilonz.gateway.routes import module

# Register Blueprints
app.register_blueprint(auth.routes.module, url_prefix='/auth')
app.register_blueprint(subsystems.routes.module, url_prefix='/subsystems')
app.register_blueprint(gateway.routes.module, url_prefix='/gateway')

# Register Error Handlers
import bilonz.routes