# Flask Components
from flask import Blueprint, request

# Internal Dependencies
from bilonz.auth.process.login import processLogin
from bilonz.auth.process.signup import processSignup

# Register Blueprint
module = Blueprint('auth', __name__)

# Signup Route
@module.route('/signup', methods=['POST'])
def create_new_user():
	data = request.get_json()
	# Process Signup
	return processSignup(data)

# Login Route
@module.route('/login', methods=['GET'])
def login():
	auth = request.authorization
	# Process Login
	return processLogin(auth)
