# Flask Components
from flask import jsonify, make_response

# Other Dependencies
from werkzeug.security import check_password_hash
import jwt
import datetime

# Internal Dependencies
from bilonz.models import User
from bilonz import db, app

# Process Login
def processLogin(auth):

	# Check if Credential(s) are Missing
	if not auth or not auth.username or not auth.password:
		# Parameters Missing
		return make_response(
			'Login Failed',
			401,
			{'WWW-Authenticate': 'Basic Realm="Missing Credentials"'}
		)
	
	# Check if user does not exist
	user = User.query.filter_by(email=auth.username).first()
	if not user:
		# User doesn't exist
		return make_response(
			'Username is Invalid',
			401,
			{'WWW-Authenticate': 'Basic Realm="User does not exist"'}
		)
	
	# Set Expiry
	expiration = datetime.datetime.utcnow() + datetime.timedelta(minutes=180)

	# Check if password is correct
	if check_password_hash(user.password, auth.password):
		# Generate and Provide Login Token
		token = jwt.encode(
			{
				'private_id': user.private_id,
				'exp': expiration
			},
			app.config['SECRET_KEY']
		)

		'''
		# Return Response
		return make_response(
			jsonify(
				{
					'token': token.decode('UTF-8'),
					'Content-Type': 'application/json; charset=utf-8'
				}
			),
			{'Set-Cookie': 'device_token = ' + token.decode('UTF-8') + '; max-age = ' + str(expiration) }
		)
		'''

		# Return Response
		responseReturn = make_response(
			jsonify(
				{
					'token': token.decode('UTF-8'),
					'Content-Type': 'application/json; charset=utf-8'
				}
			)
		)
		responseReturn.set_cookie('device_token', token.decode('UTF-8'), expires=expiration)
		return responseReturn
	
	# Incorrect Password
	return make_response(
		'Entered Password is Incorrect',
		401,
		{'WWW-Authenticate': 'Basic Realm="Incorrect password"'}
	)