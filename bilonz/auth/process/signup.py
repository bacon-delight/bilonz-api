# Flask Components
from flask import jsonify, make_response

# Other Dependencies
from werkzeug.security import generate_password_hash
import uuid
import jwt
import datetime

# Internal Dependencies
from bilonz.models import User
from bilonz import db, app

# Process Signup
def processSignup(data):

	# If parameters are missing
	if not data or not data['name'] or not data['email'] or not data['phone'] or not data['password']:
		return make_response(
			'One or Few Credentials Missing',
			401,
			{'WWW-Authenticate': 'Basic Realm="Parameters Missing"'}
		)

	# Check if user exists
	user = User.query.filter_by(email=data['email']).first()
	if user:
		return make_response(
			'User Exists with that Username',
			401,
			{'WWW-Authenticate': 'Basic Realm="User already exists"'}
		)

	hashed_password = generate_password_hash(data['password'], method='sha256')
	new_user = User(
		name=data['name'],
		email=data['email'],
		phone=data['phone'],
		password=hashed_password,
		private_id=str(uuid.uuid4())
	)

	# Commit in Database
	db.session.add(new_user)
	db.session.commit()
	
	# Set Expiry
	expiration = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
	
	# Generate and Provide Login Token
	token = jwt.encode(
		{
			'private_id': new_user.private_id,
			'exp': expiration
		},
		app.config['SECRET_KEY']
	)

	'''
	# Return Response
	return make_response(
		jsonify(
			{
				'token': token.decode('UTF-8'),
				'Content-Type': 'application/json; charset=utf-8'
			}
		),
		{'Set-Cookie': 'device_token = '+token.decode('UTF-8') }
	)
	'''

	# Return Response
	responseReturn = make_response(
		jsonify(
			{
				'token': token.decode('UTF-8'),
				'Content-Type': 'application/json; charset=utf-8'
			}
		)
	)
	responseReturn.set_cookie('device_token', token.decode('UTF-8'), expires=expiration)
	return responseReturn