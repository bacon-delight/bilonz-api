# Bilonz-APIs #

Official documentation for APIs

### Information ###

* Version: 0.1

### Server Setup Guide ###

* Initial Server Setup: https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04
* NginX Setup: https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04
* uWSGI Deployment: https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04

### Dependencies ###

* flask
* wsgi
* flask-sqlalchemy
* flask-migrate
* flask-script
* pyjwt

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact and Support ###

* Owner/Admin: Dipanjan De (dipanjan131@gmail.com)
* Google Maps API Key: AIzaSyAsOWZqm-SpE9sEa2tizRfg4VJt68GUFBI

### Git Setup ###

* GitIgnore BoilerPlate: https://www.python-boilerplate.com/py3+flask+gitignore+logging
* Fingerprint: 13:E3:32:96:E0:A9:D4:D4:63:B5:59:10:24:5A:28:90:9D:3A:8D:D0